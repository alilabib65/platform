const Passport  = require('passport');
const User      = require('../../../models/User');
module.exports={
        
    signup:async(req, res, next)=>{
        var user          = new User();
        user.name         = req.body.name;
        user.email        = req.body.email;
        user.setPassword(req.body.password); 
        var savedUSer =   await user.save();
        if(savedUSer){
            res.redirect('/login');
        }

    }

}