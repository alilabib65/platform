const nodemailer = require('nodemailer');
const Parser     = require('rss-parser');
const Link       = require('../../models/Link');
const mailconfig = require('../../helpers/mailer');
const transporter= nodemailer.createTransport(mailconfig.mailer);
const parser = new Parser();
module.exports={
    index :async(req, res, next)=>{
        
             let news = async  ()=>{
                try{
                const FEEDS = [
                  'https://www.theguardian.com/world/rss',  
                  'https://www.reddit.com/.rss',
                  'http://github-trends.ryotarai.info/rss/github_trends_javascript_daily.rss',
                  'http://feeds.bbci.co.uk/news/world/rss.xml',
                  'https://www.reddit.com/.rss',
                  'https://www.france24.com/en/rss',
                  'http://feeds.washingtonpost.com/rss/world',
                  'http://feeds.bbci.co.uk/news/world/rss.xml',
                  'https://www.reddit.com/r/worldnews/.rss',
                  'https://www.e-ir.info/category/blogs/feed/',
                  'http://www.globalissues.org/news/feed',
                  'https://www.thecipherbrief.com/feed',
                  'https://www.yahoo.com/news/rss/world'
                ];
              
                const feedRequests = FEEDS.map(async (feed)=>{
                    const data =  await parser.parseURL(feed);
                    return data;
                })

                const feedData = await Promise.all(feedRequests);
                return feedData;
                }catch(err) {
                    console.log(err);
                }
              
              }

              news()
              .then(val=>console.log(val))
              .catch(err=> console.log(err));
        res.render('home', { title: 'Muslims News' });
    },
    newNews:async(req, res, next)=>{

    },
    contact:async(req, res, next)=>{
        res.render('contact',{title:'Contact us'});
    },
    sendEmail:async(req, res, next)=>{
        var mailOptions ={
            from:'Muslim News <no-replay@muslimnews.com>',
            to:'dr.aymanezizi@gmail.com',
            subject:'You got A new Message from visitor',
            text:req.body.message
        };

        transporter.sendMail(mailOptions,function(error, info){
            if(error){
                return console.log(error)
            }

            res.render('thank',{title:'Message'});
        });

        
    },
  


}