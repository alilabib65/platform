const express            = require('express');
const router             = express.Router();
const HomeController     = require('../controllers/Frontend/HomeController');
const AuthController     = require('../controllers/Frontend/Auth/LoginController');
const RegisterController = require('../controllers/Frontend/Auth/RegisterController');
const Validtor           = require('../helpers/validation');
const passport           = require('passport');
const User               = require('../models/User'); 
/* GET Home Page. */
router.route('/')
.get(HomeController.index);

/* GET Contact Page */
router.route('/contact')
.get(HomeController.contact) 
.post(Validtor.validateBody(Validtor.schemas.contactSchema),HomeController.sendEmail);  

Validtor.schemas.userSchema

router.route('/login')
.get(AuthController.login)
.post(Validtor.validateBody(Validtor.schemas.authSchema), 
    passport.authenticate('local',{failureRedirect:'/login'}),
    AuthController.Auth);

router.route('/signup')
.post(Validtor.validateBody(Validtor.schemas.userSchema),RegisterController.signup);

router.route('/logout')
.get(AuthController.logOut);

module.exports = router;