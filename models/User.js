const mongoose = require('mongoose');
const crypto   = require('crypto-browserify');
const schema   = mongoose.Schema;
const userSchema = new schema({ 
    email:{
        type:String,
        unique:true,
        required:true
    },
    name:{
        type:String,
        required:true,
    },
    image:{
       type:String,
       default:'', 
    },
    country:{
       type:String,
       default:'' 
    },
    salt:{
       type:String 
    },
    password:{
        type:String
    },
    remember_token:{
        type:String,
        default:''
    },
    api_token:{
        type:String,
        default:''
    },
    posts:[{
        type:schema.Types.ObjectId,
        ref :'News'
    }],
    messages:[{
        type:schema.Types.ObjectId,
        ref:'Message'
    }],
    videos:[{
        type:schema.Types.ObjectId,
        ref:'Video'
    }],
    active:{
        type:Boolean,
        default:true
    }

});

userSchema.methods.setPassword = function(password){
    this.salt     =  crypto.randomBytes(16).toString('hex');
    this.password =  crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha1').toString('hex');
};

userSchema.methods.validPassword = function(password){
    const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha1').toString('hex');
    return this.password == hash;
};


const User = mongoose.model('User',userSchema);



module.exports = User;