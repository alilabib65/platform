const passport      = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User          = require('../models/User');
const Admin         = require('../models/Admin');
passport.serializeUser(function(user,done){
    done(null, user._id);
});

passport.deserializeUser(function(id,done){
    User.findOne({_id:id},function(err,user){
       done(err,user); 
    });
});

passport.use('user-signin',new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback : true
},
(req,email ,password, done)=>{
    console.log('email  '+ email + '  , paswword => ' + password);
   User.findOne({email:email},function(err,user){
       
       if(err) return done(err);
       
       if(!user){
          return done(null, false, {
            message: 'Wrong Email Or Password'
          });
       } 
       
       if(!user.validPassword(password)){
           return done(null, false, {
             message: 'Wrong password'
           });
       }
       
       return done(null, user);
   }) 
}
));


passport.use('admin-login',new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback : true
},
(req,email ,password, done)=>{
    console.log('email  '+ email + '  , paswword => ' + password);
   Admin.findOne({email:email},function(err,admin){
       
       if(err) return done(err);
       
       if(!admin){
          return done(null, false, {
            message: 'Wrong Email Or Password'
          });
       } 
       
       if(!admin.validPassword(password)){
           return done(null, false, {
             message: 'Wrong password'
           });
       }
       
       return done(null, admin);
   }) 

}
));