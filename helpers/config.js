module.exports={
    dbConnString:'mongodb://localhost:27017/platform',
    dbUrlParser :{useNewUrlParser: true},
    sessionKey:'drAymanSaifElIslam1',
    isAuthenticated:(req,res,next)=>{
       if(!req.user){
           res.redirect('/'); 
       }else{
          return next();  
       }    
    }
}