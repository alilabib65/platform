const Joi = require('@hapi/joi');

module.exports ={
    validateParam:(schema,name)=>{
        return (req, res, next)=>{
           const result = Joi.validate({params:req['params'][name]},schema);
           if(result.error){
                return res.status(400).json(result.error);
           }else{
              if(!req.value)
                req.value={};
              
              if(!req.value['params'])
                req.value['params']={};
                
              req.value['params'][name] =result.value.param;
              next();  
           } 
        }
    },

    /**
     *  req.params.userId
     *  req[params][name]
     *  req.params.newsId
     *   
     */

     validateBody:(schema)=>{
        return (req, res, next)=>{
            const result = Joi.validate(req.body,schema);
            if(result.error){
                return res.status(400).json(result.error);
            }else{
                if(!req.value)
                    req.value = {};
                if(!req.value['body'])
                    req.value['body'] ={};
                req.value['body'] = result.value;
                next();
            }
        }
     },

     schemas:{
        userSchema:Joi.object().keys({
            name  : Joi.string().required(),
            email : Joi.string().email().required(),
            password:Joi.string().required(),
            rpassword:Joi.string(),
            agree:Joi.string().required()
        }),
        userOptinalSchema:Joi.object().keys({
            name :Joi.string(),
            email    :Joi.string().email()
        }),
        idSchema:Joi.object().keys({
            params:Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
        }),
        authSchema:Joi.object().keys({
            email : Joi.string().required(),
            password :Joi.string().required()  
        }),
        contactSchema:Joi.object().keys({
            name:Joi.string(),
            email:Joi.string().email(),
            message:Joi.string()
        })
     }     
}